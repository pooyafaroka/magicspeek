﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace MagicSpeek.GoogleHelper
{
    public class GoogleInterface
    {
        private String SPEECH_RECOGNIZE_API_KEY;
        private String SPEECH_RECOGNIZE_API = "https://speech.googleapis.com/v1/speech:recognize?";

        public GoogleInterface(String SPEECH_RECOGNIZE_API_KEY)
        {
            this.SPEECH_RECOGNIZE_API_KEY = SPEECH_RECOGNIZE_API_KEY;
        }

        private GoogleTypes.X_JObjectResult CallApiMethod(String uri, dynamic Parameters_Object)
        {
            GoogleTypes.X_JObjectResult retValue = new GoogleTypes.X_JObjectResult();
            try
            {
                WebRequest req = (HttpWebRequest)HttpWebRequest.Create(uri);
                req.Method = "POST";
                req.ContentType = "application/json";
                req.Credentials = CredentialCache.DefaultCredentials;

                string json = "";
                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    json = JsonConvert.SerializeObject(Parameters_Object, Newtonsoft.Json.Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                HttpWebResponse hwrResponse = (HttpWebResponse)req.GetResponse();
                StreamReader srResponse = new StreamReader(hwrResponse.GetResponseStream());
                retValue = JsonConvert.DeserializeObject<GoogleTypes.X_JObjectResult>(srResponse.ReadToEnd());
            }
            catch (WebException ex)
            {
                var webResponse = ex.Response as HttpWebResponse;
            }
            return retValue;
        }

        /////////////////////////////////// All Google method start from here ///////////////////////////////////

        internal GoogleTypes.X_Json_Google_Speech_Result VoiceToSpeech(string file_path)
        {
            GoogleTypes.X_Json_Google_Speech_Result x_json_Google_speech_result = new GoogleTypes.X_Json_Google_Speech_Result();
            try
            {
                WebRequest req = HttpWebRequest.Create(file_path);
                Stream streamF = req.GetResponse().GetResponseStream();

                if (streamF != null)
                {
                    //Stream voice
                    MemoryStream memoryStream = new MemoryStream();
                    streamF.CopyTo(memoryStream);
                    byte[] baAudioFile = memoryStream.ToArray();
                    String fileData = Convert.ToBase64String(baAudioFile);

                    //Setup config and speech
                    GoogleTypes.Action_Json_Speech_Config speech_config = new GoogleTypes.Action_Json_Speech_Config();
                    GoogleTypes.Action_Json_Speech_Config.Config config = new GoogleTypes.Action_Json_Speech_Config.Config();
                    GoogleTypes.Action_Json_Speech_Config.Audio audio = new GoogleTypes.Action_Json_Speech_Config.Audio();
                    config.encoding = GoogleTypes.AudioFormat.OGG_OPUS;
                    config.languageCode = GoogleTypes.LanguageCode.en_US;
                    config.sampleRateHertz = GoogleTypes.SampleRateHertz.RATE_16000;
                    config.maxAlternatives = 10;
                    audio.content = fileData;
                    speech_config.audio = audio;
                    speech_config.config = config;

                    String URL_REQ = SPEECH_RECOGNIZE_API + SPEECH_RECOGNIZE_API_KEY;

                    GoogleTypes.X_JObjectResult feedback = CallApiMethod(URL_REQ, speech_config);
                    List<JObject> jObjFeedback = feedback.results as List<JObject>;
                    x_json_Google_speech_result = jObjFeedback[0].ToObject<GoogleTypes.X_Json_Google_Speech_Result>();
                }
            }
            catch (Exception ex)
            {

            }
            return x_json_Google_speech_result;
        }


    }
}