﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagicSpeek.GoogleHelper
{
    public class GoogleTypes
    {
        #region Constance Class Type
        public class AudioFormat
        {
            public static string ENCODING_UNSPECIFIED = "ENCODING_UNSPECIFIED";
            public static string LINEAR16 = "LINEAR16";
            public static string FLAC = "FLAC";
            public static string MULAW = "MULAW";
            public static string AMR = "AMR";
            public static string AMR_WB = "AMR_WB";
            public static string OGG_OPUS = "OGG_OPUS";
            public static string SPEEX_WITH_HEADER_BYTE = "SPEEX_WITH_HEADER_BYTE";
        }

        public class LanguageCode
        {
            public static string en_US = "en-US";
        }

        public class SampleRateHertz
        {
            public static int RATE_16000 = 16000;
        }

        #endregion

        #region Class Type

        public class X_JObjectResult
        {
            public List<JObject> results;
        }

        #endregion

        #region Return Values in JSON

        public class X_Json_Google_Speech_Result
        {
            public List<Alternative> alternatives { get; set; }

            public class Alternative
            {
                public string transcript { get; set; }
                public double confidence { get; set; }
            }
        }

        #endregion

        #region Send Google Method Parameters

        public class Action_Json_Speech_Config
        {
            public Config config { get; set; }
            public Audio audio { get; set; }

            public class Config
            {
                public string encoding { get; set; }
                public int sampleRateHertz { get; set; }
                public string languageCode { get; set; }
                public int maxAlternatives { get; set; }
            }

            public class Audio
            {
                public string content { get; set; }
            }

        }

        #endregion
    }
}