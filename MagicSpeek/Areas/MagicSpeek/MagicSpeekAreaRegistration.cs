﻿using System.Web.Mvc;

namespace MagicSpeek.Areas.MagicSpeek
{
    public class MagicSpeekAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MagicSpeek";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MagicSpeek_default",
                "MagicSpeek/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}