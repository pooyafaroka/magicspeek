﻿using MagicSpeek.GoogleHelper;
using MagicSpeek.Models;
using MagicSpeek.TelegramHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;


namespace MagicSpeek.Areas.MagicSpeek.Controllers
{
    /// <summary>
    /// Webhook: https://api.telegram.org/bot388101817:AAHVLLBn-so1XDlGfCijzYsXIMgLF3JxG6Y/SetWebHook?url=https://pooyafaroka.localtunnel.me/MagicSpeek/Bot/
    /// Port: 2541
    /// ngrok: ngrok http 2541 -host-header="localhost:2541"
    /// NPM: iis-lt --port 2541 --subdomain pooyafaroka
    /// GetWebHookInfo: https://api.telegram.org/bot388101817:AAHVLLBn-so1XDlGfCijzYsXIMgLF3JxG6Y/GetWebHookInfo
    /// </summary>
    public class BotController : Controller, IMessageProcessor
    {
        public TelegramInterface _telegram_interface;
        ResourceManager rm;
        private int TRUE_ANSWER_POINT = 5;
        private int GOOGLE_POINT_5 = 5;
        private int GOOGLE_POINT_4 = 4;
        private int GOOGLE_POINT_3 = 3;
        private int GOOGLE_POINT_2 = 2;

        public ActionResult Index()
        {
            rm = new ResourceManager("MagicSpeek.ResourceString", Assembly.GetExecutingAssembly());
            _telegram_interface = new TelegramInterface(WebConfigurationManager.AppSettings["TELEGRAM_dor_o_bar_bot_API_KEY"]);
            _telegram_interface.ParseUpdate(Request.InputStream, this);

            return Content("{\"ok\":\"false\", \"result\":\"No data\"}", "application/json");
        }

        public String OnRecievedInlineQuery(TelegramTypes.X_Update xCallback)
        {
            return null;
        }

        public String OnRecievedCallbackQuery(TelegramTypes.X_Update xCallback)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            long chat_id = xCallback.callback_query.from.id;
            long message_id = xCallback.callback_query.message.message_id;
            String str_current_level = "";
            String str_current_sublevel = "";
            int i_current_level = 0;
            int i_current_sublevel = 0;
            int points = 0;

            if (xCallback.callback_query.data.Contains("LEVEL"))
            {
                switch (xCallback.callback_query.data)
                {
                    case "LEVEL.NEXT":
                        bool f_isNewUser = false;
                        ParseImageLableLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                        i_current_level = Convert.ToInt16(str_current_level);
                        i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                        if (i_current_sublevel != 20)
                        {
                            i_current_sublevel++;
                        }
                        else
                        {
                            i_current_sublevel = 1;
                        }
                        DBHelper.UserSateModel userState = DBHelper.getUserState(chat_id, 0, xCallback.callback_query.from.first_name, xCallback.callback_query.from.last_name, xCallback.callback_query.from.username, out f_isNewUser);
                        HandleLevelMenuByPoint(xCallback, i_current_level, i_current_sublevel, userState.Point[i_current_level - 1][i_current_sublevel - 1]);

                        break;

                    case "LEVEL.PREV":
                        ParseImageLableLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                        i_current_level = Convert.ToInt16(str_current_level);
                        i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                        if (i_current_sublevel != 1)
                        {
                            i_current_sublevel--;
                        }
                        else
                        {
                            i_current_sublevel = 20;
                        }
                        userState = DBHelper.getUserState(chat_id, 0, xCallback.callback_query.from.first_name, xCallback.callback_query.from.last_name, xCallback.callback_query.from.username, out f_isNewUser);
                        HandleLevelMenuByPoint(xCallback, i_current_level, i_current_sublevel, userState.Point[i_current_level - 1][i_current_sublevel - 1]);

                        break;

                    case "LEVEL.SELECT":
                        ParseImageLableLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                        DBHelper.SaveLevelInfo(chat_id, xCallback, str_current_level, str_current_sublevel);

                        i_current_level = Convert.ToInt16(str_current_level);
                        i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                        List<String> conversionLines = DBHelper.getConversation(i_current_level, i_current_sublevel);

                        String wholeConversion = "#Conversion #Level_" + i_current_level.ToString() + " #Sublevel_" + i_current_sublevel.ToString() + "\n";
                        for (int i = 0; i < conversionLines.Count; i++)
                        {
                            wholeConversion += "🔸" + conversionLines[i] + "\n\n";
                        }
                        inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                        inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                        {
                            text = "Watch dialog video",
                            callback_data = "WHOLE_VOICE"
                        });
                        inlineKeyboardMarkup.EndRow();
                        _telegram_interface.SendMessage(chat_id, wholeConversion, inlineKeyboardMarkup);
                        break;
                }
            }
            else if (xCallback.callback_query.data.Contains("WHOLE_VOICE"))
            {
                ParseHashTagedLevel(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel);
                i_current_level = Convert.ToInt16(str_current_level);
                i_current_sublevel = Convert.ToInt16(str_current_sublevel);
                //String conversionVoice = DBHelper.getConversionVoice(chat_id, i_current_level, i_current_sublevel);
                String conversionVideo = DBHelper.getConversionVideo(chat_id, IntToString(i_current_level), IntToString(i_current_sublevel));
                inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                {
                    text = "Finish Watch Dialog",
                    callback_data = "DIALOG_VIDEO.FINISH"
                });

                inlineKeyboardMarkup.EndRow();
                //_telegram_interface.SendVoice(conversionVoice, chat_id, "#Voice " + "#Level_" + i_current_level + " #Sublevel_" + i_current_sublevel, inlineKeyboardMarkup);
                _telegram_interface.SendVideo(chat_id, conversionVideo, 320, 640, "#Video " + "#Level_" + i_current_level + " #Sublevel_" + i_current_sublevel, inlineKeyboardMarkup);
            }
            else if (xCallback.callback_query.data.Contains("DIALOG_VIDEO"))
            {
                switch (xCallback.callback_query.data)
                {
                    case "DIALOG_VIDEO.FINISH":
                        String str_total_count = "";
                        message_id = xCallback.callback_query.message.message_id;
                        ParseVideoCallback(xCallback.callback_query.message.caption, out str_current_level, out str_current_sublevel);
                        TelegramTypes.Action_ForceReply force_replay_msg = new TelegramTypes.Action_ForceReply
                        {
                            force_reply = true
                        };
                        List<String> Whole_Conversion = DBHelper.getConversation(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel));
                        _telegram_interface.SendMessage(chat_id,
                            "🔸 Level: " + str_current_level + "_" + str_current_sublevel + "\n🎤 Repeat A: " + Whole_Conversion.Count.ToString() + '/' + "01"
                            + "\n\n"
                            + "متن زیر را تکرار کنید" + "\n" + Whole_Conversion[0]
                            , force_replay_msg, false, false, 0, "HTML");
                        
                        break;
                }
            }
            else if (xCallback.callback_query.data.Contains("GOOGLE.SPEECH.REPEAT_AFTER_ME"))
            {
                switch (xCallback.callback_query.data)
                {
                    case "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_A":
                        String str_current_part = "";
                        ParseRepeatCallback(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_current_part);
                        int iCurrent_level = Convert.ToInt16(str_current_level);
                        int iCurrent_sublevel = Convert.ToInt16(str_current_sublevel);
                        String str_total_count = IntToString(DBHelper.getConversation(iCurrent_level, iCurrent_sublevel).Count);
                        TelegramTypes.Action_ForceReply force_replay_msg = new TelegramTypes.Action_ForceReply
                        {
                            force_reply = true
                        };
                        _telegram_interface.SendMessage(chat_id,
                            "🔸 Level: " + str_current_level + "_" + str_current_sublevel + "\n🎤 Repeat B: " + str_total_count + '/' + "01"
                            + "\n\n"
                            + "به این جمله پاسخ مناسب دهید" + "\n" + DBHelper.getConversionLine(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), 1)
                            , force_replay_msg, false, false, 0, "HTML");
                        break;

                    case "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_B":
                        str_current_part = "";
                        ParseRepeatCallback(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_current_part);
                        iCurrent_level = Convert.ToInt16(str_current_level);
                        iCurrent_sublevel = Convert.ToInt16(str_current_sublevel);
                        str_total_count = IntToString(DBHelper.getConversation(iCurrent_level, iCurrent_sublevel).Count);
                        force_replay_msg = new TelegramTypes.Action_ForceReply
                        {
                            force_reply = true
                        };
                        _telegram_interface.SendMessage(chat_id,
                            "🔸 Level: " + str_current_level + "_" + str_current_sublevel + "\n🎤 Repeat C: " + str_total_count + '/' + "01"
                            + "\n\n"
                            + "جمله زیر را تکرار کنید." + "\n" + DBHelper.getConversionLine(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), 1)
                            , force_replay_msg, false, false, 0, "HTML");
                        break;

                    case "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_C":
                        break;

                    case "GOOGLE.SPEECH.REPEAT_AFTER_ME.NEXT":
                        break;

                    case "GOOGLE.SPEECH.REPEAT_AFTER_ME.PREV":
                        break;

                    case "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH":
                        String str_current_line = "";
                        str_total_count = "";
                        message_id = xCallback.callback_query.message.message_id;
                        ParseRepeatCallback(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_current_part);
                        inlineKeyboardMarkup = CreateQuizeMenu();
                        List<String> Quize = DBHelper.GetQuizeInfo(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), 0);
                        _telegram_interface.SendMessage(chat_id, CreateQuize(Quize, str_current_level, str_current_sublevel), inlineKeyboardMarkup);
                        break;

                    default:
                        _telegram_interface.SendMessage(chat_id, "Please replay on SENTENCE you want to repear.");
                        break;
                }
            }
            else if (xCallback.callback_query.data.Contains("QUIZE"))
            {
                String str_current_quize = "";
                String str_total_quize = "";
                int Answer = 0;
                int i_current_quize = 0;
                int i_total_quize = 0;
                ParseQuize(xCallback.callback_query.message.text, out str_current_level, out str_current_sublevel, out str_current_quize, out str_total_quize);
                i_current_quize = Convert.ToInt16(str_current_quize);
                i_total_quize = Convert.ToInt16(str_total_quize);
                switch (xCallback.callback_query.data)
                {
                    case "QUIZE.1":
                        Answer = 0;
                        break;

                    case "QUIZE.2":
                        Answer = 1;
                        break;

                    case "QUIZE.3":
                        Answer = 2;
                        break;

                    case "QUIZE.4":
                        Answer = 3;
                        break;
                }
                List<String> Quize = DBHelper.GetQuizeInfo(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), Convert.ToInt16(str_current_quize) - 1);
                _telegram_interface.editMessageText(chat_id, message_id, CheckQuize(chat_id, xCallback.message.from, Quize, str_current_level, str_current_sublevel, Answer));

                if(i_current_quize == i_total_quize)
                {
                    _telegram_interface.SendMessage(chat_id, "💠💠💠💠💠💠💠💠💠💠💠");
                }
                else
                {
                    Quize = DBHelper.GetQuizeInfo(Convert.ToInt16(str_current_level), Convert.ToInt16(str_current_sublevel), Convert.ToInt16(str_current_quize));
                    inlineKeyboardMarkup = CreateQuizeMenu();
                    _telegram_interface.SendMessage(chat_id, CreateQuize(Quize, str_current_level, str_current_sublevel), inlineKeyboardMarkup);
                }
            }
            return null;
        }

        public String OnRecievedVoice(TelegramTypes.X_Update xCallback)
        {
            long chat_id = xCallback.message.chat.id;
            try
            {
                if (xCallback.message.reply_to_message != null)
                {
                    #region xCallback.message.reply_to_message != null
                    if (xCallback.message.reply_to_message.text.Contains("🎤 Repeat A: "))
                    {
                        String main_sentence = xCallback.message.reply_to_message.text.Split('\n')[4].Replace("?", "");
                        if (main_sentence.Contains(":"))
                        {
                            main_sentence = main_sentence.Split(':')[1];
                        }
                        String current_level = "";
                        String current_sublevel = "";
                        String current_repeat = "";
                        String total_repeat = "";
                        ParseRepeat(xCallback.message.reply_to_message.text, out current_level, out current_sublevel, out current_repeat, out total_repeat);
                        int iTotal_repeat = Convert.ToInt16(total_repeat);
                        int iCurrent_repeat = Convert.ToInt16(current_repeat);

                        //Get voice file path from telegram
                        String VoiceFileId = xCallback.message.voice.file_id;
                        String VoiceFilePath = _telegram_interface.getFilePath(VoiceFileId);

                        //Call google speech recognation engine
                        GoogleInterface _google_interface = new GoogleInterface(WebConfigurationManager.AppSettings["GOOGLE_SPEECH_API_KEY"]);
                        GoogleTypes.X_Json_Google_Speech_Result google_result = _google_interface.VoiceToSpeech(VoiceFilePath);
                        if(google_result.alternatives != null)
                        {
                            String UserSentence = getMaxConfidenceSentence(google_result);
                            int distance = CalcLevenshteinDistance(UserSentence, main_sentence);

                            String msg = "";
                            if (distance <= 4)
                            {
                                msg = "Awesome! Repeat next part.";
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else if (distance <= 7 && distance >= 5)
                            {
                                msg = "Great! Repeat next part.";
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else if (distance <= 10 && distance >= 8)
                            {
                                msg = "Good! Repeat next part.";
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else
                            {
                                msg = "Not bad! Try again.";
                            }
                            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = null;
                            if (iCurrent_repeat > iTotal_repeat)
                            {
                                inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                                {
                                    text = "FINISH PART A",
                                    callback_data = "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_A"
                                });
                                inlineKeyboardMarkup.EndRow();
                            }

                            _telegram_interface.SendMessage(chat_id, msg);
                            getVoiceOfDialogWithForceReplay(chat_id, current_level, current_sublevel, "A", total_repeat, current_repeat, inlineKeyboardMarkup);
                        }
                        else
                        {
                            _telegram_interface.SendMessage(chat_id, "NotImplementedException: NI020\n" + "(Google Result) value is NULL");
                        }
                    }
                    else if (xCallback.message.reply_to_message.text.Contains("🎤 Repeat B: "))
                    {
                        TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = null;
                        String main_sentence = "";
                        String current_level = "";
                        String current_sublevel = "";
                        String current_repeat = "";
                        String total_repeat = "";
                        String msg = "";

                        ParseRepeat(xCallback.message.reply_to_message.text, out current_level, out current_sublevel, out current_repeat, out total_repeat);
                        int iTotal_repeat = Convert.ToInt16(total_repeat);
                        int iCurrent_repeat = Convert.ToInt16(current_repeat);
                        if (iCurrent_repeat < iTotal_repeat)
                        {
                            main_sentence = DBHelper.getConversionLine(Convert.ToInt16(current_level), Convert.ToInt16(current_sublevel), iCurrent_repeat + 1);
                            if (main_sentence.Contains(":"))
                            {
                                main_sentence = main_sentence.Split(':')[1];
                            }
                            //Get voice file path from telegram
                            String VoiceFileId = xCallback.message.voice.file_id;
                            String VoiceFilePath = _telegram_interface.getFilePath(VoiceFileId);

                            //Call google speech recognation engine
                            GoogleInterface _google_interface = new GoogleInterface(WebConfigurationManager.AppSettings["GOOGLE_SPEECH_API_KEY"]);
                            GoogleTypes.X_Json_Google_Speech_Result google_result = _google_interface.VoiceToSpeech(VoiceFilePath);
                            String UserSentence = getMaxConfidenceSentence(google_result);
                            int distance = CalcLevenshteinDistance(UserSentence, main_sentence);

                            if (distance <= 4)
                            {
                                msg = "Awesome! Answer next part.";
                                iCurrent_repeat++;
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else if (distance <= 7 && distance >= 5)
                            {
                                msg = "Great! Answer next part.";
                                iCurrent_repeat++;
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else if (distance <= 10 && distance >= 8)
                            {
                                msg = "Good! Answer next part.";
                                iCurrent_repeat++;
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else
                            {
                                msg = "Not bad! Try again.\nYou must say: \n\n💠 " + main_sentence;
                            }
                        }
                        else
                        {
                            _telegram_interface.SendMessage(chat_id, "NotImplemented: NI004");
                        }
                        inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                        inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                        {
                            text = "FINISH PART B",
                            callback_data = "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_B"
                        });
                        inlineKeyboardMarkup.EndRow();


                        _telegram_interface.SendMessage(chat_id, msg);
                        getVoiceOfDialogWithForceReplay(chat_id, current_level, current_sublevel, "B", total_repeat, current_repeat, inlineKeyboardMarkup);
                    }
                    else if (xCallback.message.reply_to_message.text.Contains("🎤 Repeat C: "))
                    {
                        if (xCallback.message.reply_to_message.text.Contains("/01"))
                        {
                            String main_sentence = xCallback.message.reply_to_message.text.Split('\n')[4].Replace("?", "");
                            if (main_sentence.Contains(":"))
                            {
                                main_sentence = main_sentence.Split(':')[1];
                            }
                            String current_level = "";
                            String current_sublevel = "";
                            String current_repeat = "";
                            String total_repeat = "";
                            ParseRepeat(xCallback.message.reply_to_message.text, out current_level, out current_sublevel, out current_repeat, out total_repeat);
                            int iTotal_repeat = Convert.ToInt16(total_repeat);
                            int iCurrent_repeat = Convert.ToInt16(current_repeat);

                            //Get voice file path from telegram
                            String VoiceFileId = xCallback.message.voice.file_id;
                            String VoiceFilePath = _telegram_interface.getFilePath(VoiceFileId);

                            //Call google speech recognation engine
                            GoogleInterface _google_interface = new GoogleInterface(WebConfigurationManager.AppSettings["GOOGLE_SPEECH_API_KEY"]);
                            GoogleTypes.X_Json_Google_Speech_Result google_result = _google_interface.VoiceToSpeech(VoiceFilePath);
                            String UserSentence = getMaxConfidenceSentence(google_result);
                            int distance = CalcLevenshteinDistance(UserSentence, main_sentence);

                            String msg = "";
                            if (distance <= 4)
                            {
                                msg = "Awesome! Repeat next part.";
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else if (distance <= 7 && distance >= 5)
                            {
                                msg = "Great! Repeat next part.";
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else if (distance <= 10 && distance >= 8)
                            {
                                msg = "Good! Repeat next part.";
                                iCurrent_repeat++;
                                current_repeat = IntToString(iCurrent_repeat);
                            }
                            else
                            {
                                msg = "Not bad! Try again.\nYou must say: \n\n💠 " + main_sentence;
                            }
                            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = null;
                            if (iCurrent_repeat > iTotal_repeat)
                            {
                                inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                                {
                                    text = "FINISH PART C",
                                    callback_data = "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_C"
                                });
                                inlineKeyboardMarkup.EndRow();
                            }

                            _telegram_interface.SendMessage(chat_id, msg);
                            getVoiceOfDialogWithForceReplay(chat_id, current_level, current_sublevel, "C", total_repeat, current_repeat, inlineKeyboardMarkup);
                        }
                        else
                        {
                            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = null;
                            String main_sentence = "";
                            String current_level = "";
                            String current_sublevel = "";
                            String current_repeat = "";
                            String total_repeat = "";
                            String msg = "";

                            ParseRepeat(xCallback.message.reply_to_message.text, out current_level, out current_sublevel, out current_repeat, out total_repeat);
                            int iTotal_repeat = Convert.ToInt16(total_repeat);
                            int iCurrent_repeat = Convert.ToInt16(current_repeat);
                            if (iCurrent_repeat < iTotal_repeat)
                            {
                                main_sentence = DBHelper.getConversionLine(Convert.ToInt16(current_level), Convert.ToInt16(current_sublevel), iCurrent_repeat + 1);
                                if (main_sentence.Contains(":"))
                                {
                                    main_sentence = main_sentence.Split(':')[1];
                                }
                                //Get voice file path from telegram
                                String VoiceFileId = xCallback.message.voice.file_id;
                                String VoiceFilePath = _telegram_interface.getFilePath(VoiceFileId);

                                //Call google speech recognation engine
                                GoogleInterface _google_interface = new GoogleInterface(WebConfigurationManager.AppSettings["GOOGLE_SPEECH_API_KEY"]);
                                GoogleTypes.X_Json_Google_Speech_Result google_result = _google_interface.VoiceToSpeech(VoiceFilePath);
                                String UserSentence = getMaxConfidenceSentence(google_result);
                                int distance = CalcLevenshteinDistance(UserSentence, main_sentence);

                                if (distance <= 4)
                                {
                                    msg = "Awesome! Answer next part.";
                                    iCurrent_repeat++;
                                    iCurrent_repeat++;
                                    current_repeat = IntToString(iCurrent_repeat);
                                }
                                else if (distance <= 7 && distance >= 5)
                                {
                                    msg = "Great! Answer next part.";
                                    iCurrent_repeat++;
                                    iCurrent_repeat++;
                                    current_repeat = IntToString(iCurrent_repeat);
                                }
                                else if (distance <= 10 && distance >= 8)
                                {
                                    msg = "Good! Answer next part.";
                                    iCurrent_repeat++;
                                    iCurrent_repeat++;
                                    current_repeat = IntToString(iCurrent_repeat);
                                }
                                else
                                {
                                    msg = "Not bad! Try again.\nYou must say: \n\n💠 " + main_sentence;
                                }
                            }
                            else
                            {
                                _telegram_interface.SendMessage(chat_id, "NotImplemented: NI005");
                            }
                            if (iCurrent_repeat != iTotal_repeat)
                            {
                                inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                                {
                                    text = "FINISH PART C",
                                    callback_data = "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_C"
                                });
                                inlineKeyboardMarkup.EndRow();
                                _telegram_interface.SendMessage(chat_id, msg);
                                getVoiceOfDialogWithForceReplay(chat_id, current_level, current_sublevel, "C", total_repeat, current_repeat, inlineKeyboardMarkup);
                            }
                            else
                            {
                                inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                                {
                                    text = "LET'S EXAM",
                                    callback_data = "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH"
                                });
                                inlineKeyboardMarkup.EndRow();
                                getVoiceOfDialogWithForceReplay(chat_id, current_level, current_sublevel, "C", total_repeat, current_repeat, inlineKeyboardMarkup);
                                _telegram_interface.SendMessage(chat_id,
                                    "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + "C"
                                    + "\n\n"
                                    + "مرحله سوم هم با موفقیت به پایان رسید در مرحله بعدی باید توی امتحان شرکت کنی.", inlineKeyboardMarkup);
                            }


                        }
                    }
                    else
                    {
                        _telegram_interface.SendMessage(chat_id, "NotImplemented: NI006");
                    }
                    #endregion
                }
                else
                {
                    bool f_isNewUser = false;
                    DBHelper.UserSateModel userState = DBHelper.getUserState(chat_id, 0, xCallback.message.from.first_name, xCallback.message.from.last_name, xCallback.message.from.username, out f_isNewUser);
                    if(f_isNewUser)
                    {
                        ParseGoogleVoice(chat_id, userState, xCallback.message.voice.file_id);
                    }
                    else
                    {
                        _telegram_interface.SendMessage(chat_id, "NotImplemented: NI115\n" + "(Reply to message) value is NULL and f_isNewUser is false.");
                    }
                }
            }
            catch(Exception ex)
            {
                _telegram_interface.SendMessage(chat_id, "NotImplementedException: NI002\n" + ex.Message);
            }
            
            return null;
        }

        public String OnRecievedAudio(TelegramTypes.X_Update xCallback)
        {
            return null;
        }

        public String OnRecievedVideo(TelegramTypes.X_Update xCallback)
        {
            return null;
        }

        public String OnRecievedVideoNote(TelegramTypes.X_Update xCallback)
        {
            return null;
        }

        public String OnRecievedDocument(TelegramTypes.X_Update xCallback)
        {
            return null;
        }

        public String OnRecievedPhoto(TelegramTypes.X_Update xCallback)
        {
            return null;
        }

        public String OnRecievedContact(TelegramTypes.X_Update xMessage)
        {
            return null;
        }

        public String OnRecievedLocation(TelegramTypes.X_Update xMessage)
        {
            return null;
        }

        public String OnRecievedVenue(TelegramTypes.X_Update xMessage)
        {
            return null;
        }

        public String OnRecievedMessage(TelegramTypes.X_Update xMessage)
        {
            long chat_id = xMessage.message.chat.id;
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = CreateLevelMenu(true);

            if(xMessage.message.text.Equals("/start"))
            {
                bool f_isNewUser = false;
                DBHelper.UserSateModel userState = DBHelper.getUserState(chat_id, 0, xMessage.message.from.first_name, xMessage.message.from.last_name, xMessage.message.from.username, out f_isNewUser);
                if(f_isNewUser)
                {
                    userState = new DBHelper.UserSateModel();
                    List<List<int>> newPoints = new List<List<int>>();
                    List<int> SubLevelPoints = new List<int>();
                    for (int i = 0; i < 20; i++)
                    {
                        SubLevelPoints.Add(-1);
                    }
                    for (int i = 0; i < 10; i++)
                    {
                        newPoints.Add(SubLevelPoints);
                    }
                    SubLevelPoints[0] = 0;
                    SubLevelPoints[1] = 0;
                    newPoints[0] = SubLevelPoints;
                    _telegram_interface.SendMessage(chat_id, CreateLevelImageUrl(chat_id, 1, 1, 0), inlineKeyboardMarkup, false, false, 0, "HTML");
                    DBHelper.UserSateModel newState = new DBHelper.UserSateModel();
                    DBHelper.LevelInfo levelInfo = new DBHelper.LevelInfo();
                    levelInfo.Level = 1;
                    levelInfo.SubLevel = 1;
                    levelInfo.Line = 1;
                    newState.LevelInfo = levelInfo;
                    newState.Point = newPoints;
                    newState.LevelName = "NaN";
                    
                    DBHelper.setUserState(chat_id, newState);
                }
                else
                {
                    _telegram_interface.SendMessage(chat_id, CreateLevelImageUrl(chat_id, userState.LevelInfo.Level, userState.LevelInfo.SubLevel, userState.Point[userState.LevelInfo.Level - 1][userState.LevelInfo.SubLevel - 1]), inlineKeyboardMarkup, false, false, 0, "HTML");
                }
            }
            else if(xMessage.message.text.Equals("/menu"))
            {
                //Apeare menu by image
            }
            else if (xMessage.message.text.Contains("?"))
            {
                
            }
            else
            {
                _telegram_interface.SendMessage(chat_id, rm.GetString("HELP"));
            }

            return null;
        }

        private void ParseGoogleVoice(long chat_id, DBHelper.UserSateModel userState, String VoiceFileId)
        {
            String Part = userState.LevelName;
            if(Part.Equals("NaN"))
            {
                List<String> Whole_sentence = DBHelper.getConversation(userState.LevelInfo.Level, userState.LevelInfo.SubLevel);
                String main_sentence = Whole_sentence[userState.LevelInfo.Line - 1];// DBHelper.getConversionLine(userState.LevelInfo.Level, userState.LevelInfo.SubLevel, userState.LevelInfo.Line);
                String current_repeat = "";
                if (main_sentence.Contains(":"))
                {
                    main_sentence = main_sentence.Split(':')[1];
                }
                int iTotal_repeat = Convert.ToInt16(Whole_sentence.Count);
                int iCurrent_repeat = Convert.ToInt16(userState.LevelInfo.Line);

                //Get voice file path from telegram
                String VoiceFilePath = _telegram_interface.getFilePath(VoiceFileId);

                //Call google speech recognation engine
                GoogleInterface _google_interface = new GoogleInterface(WebConfigurationManager.AppSettings["GOOGLE_SPEECH_API_KEY"]);
                GoogleTypes.X_Json_Google_Speech_Result google_result = _google_interface.VoiceToSpeech(VoiceFilePath);
                if (google_result.alternatives != null)
                {
                    String UserSentence = getMaxConfidenceSentence(google_result);
                    int distance = CalcLevenshteinDistance(UserSentence, main_sentence);

                    String msg = "";
                    if (distance <= 4)
                    {
                        msg = "Awesome! Repeat next part.";
                        iCurrent_repeat++;
                        current_repeat = IntToString(iCurrent_repeat);
                    }
                    else if (distance <= 7 && distance >= 5)
                    {
                        msg = "Great! Repeat next part.";
                        iCurrent_repeat++;
                        current_repeat = IntToString(iCurrent_repeat);
                    }
                    else if (distance <= 10 && distance >= 8)
                    {
                        msg = "Good! Repeat next part.";
                        iCurrent_repeat++;
                        current_repeat = IntToString(iCurrent_repeat);
                    }
                    else
                    {
                        msg = "Not bad! Try again.";
                    }
                    TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = null;
                    if (iCurrent_repeat > iTotal_repeat)
                    {
                        inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
                        inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                        {
                            text = "FINISH PART A",
                            callback_data = "GOOGLE.SPEECH.REPEAT_AFTER_ME.FINISH_PART_A"
                        });
                        inlineKeyboardMarkup.EndRow();
                    }

                    _telegram_interface.SendMessage(chat_id, msg);
                    getVoiceOfDialogWithForceReplay(chat_id, userState.LevelInfo.Level.ToString(), userState.LevelInfo.SubLevel.ToString(), "A", Whole_sentence.Count.ToString(), current_repeat, inlineKeyboardMarkup);
                }
                else
                {
                    _telegram_interface.SendMessage(chat_id, "NotImplementedException: NI020\n" + "(Google Result) value is NULL");
                }
            }
            else if(Part.Equals("PART_B"))
            {

            }
            else if (Part.Equals("PART_C"))
            {

            }
        }

        /// <summary>
        /// Create level menu with glassly button.
        /// </summary>
        /// <param name="xUpdate"></param>
        private TelegramTypes.X_InlineKeyboardMarkup CreateLevelMenu(bool hasSelectButton)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "PREVIEW",
                callback_data = "LEVEL.PREV"
            });

            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "NEXT",
                callback_data = "LEVEL.NEXT"
            });

            inlineKeyboardMarkup.EndRow();
            if(hasSelectButton)
            {
                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                {
                    text = "SELECT LEVEL",
                    callback_data = "LEVEL.SELECT"
                });
                inlineKeyboardMarkup.EndRow();
            }
            
            return inlineKeyboardMarkup;
        }

        /// <summary>
        /// Create a menu for listening void line by line.
        /// </summary>
        /// <param name="hasFinishButton"></param>
        /// <returns></returns>
        private TelegramTypes.X_InlineKeyboardMarkup CreateVoiceMenu(bool hasFinishButton)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "PREVIEW",
                callback_data = "SINGLE_VOICE.PREV"
            });

            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "NEXT",
                callback_data = "SINGLE_VOICE.NEXT"
            });

            inlineKeyboardMarkup.EndRow();
            if (hasFinishButton)
            {
                inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
                {
                    text = "FINISH",
                    callback_data = "SINGLE_VOICE.FINISH"
                });
                inlineKeyboardMarkup.EndRow();
            }

            return inlineKeyboardMarkup;
        }

        /// <summary>
        /// Create Quize Menu
        /// </summary>
        /// <returns></returns>
        private TelegramTypes.X_InlineKeyboardMarkup CreateQuizeMenu()
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "1",
                callback_data = "QUIZE.1"
            });
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "2",
                callback_data = "QUIZE.2"
            });
            inlineKeyboardMarkup.EndRow();
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "3",
                callback_data = "QUIZE.3"
            });
            inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            {
                text = "4",
                callback_data = "QUIZE.4"
            });
            inlineKeyboardMarkup.EndRow();

            return inlineKeyboardMarkup;
        }

        private String CreateQuize(List<String> Quize, String current_level, String current_sublevel)
        {
            String retValue = "🔸 Level: " + current_level + "_" + current_sublevel + "," + " Quize: " + Quize[0] + "\n";
            retValue += "💠 " + Quize[1] + "\n"
                        + "\n1️⃣ " + Quize[2]
                        + "\n2️⃣ " + Quize[3]
                        + "\n3️⃣ " + Quize[4]
                        + "\n4️⃣ " + Quize[5];

            return retValue;
        }

        /// <summary>
        /// This method check user answer to the quise, and create true and false emoji.
        /// </summary>
        /// <param name="Quize"></param>
        /// <param name="current_level"></param>
        /// <param name="current_sublevel"></param>
        /// <param name="Answer"></param>
        /// <returns></returns>
        private String CheckQuize(long chat_id, TelegramTypes.X_User x_User, List<String> Quize, String current_level, String current_sublevel, int Answer)
        {
            bool f_isNewUser = false;
            DBHelper.UserSateModel oldUserData = DBHelper.getUserState(chat_id, 0, x_User.first_name, x_User.last_name, x_User.username, out f_isNewUser);
            int last_point = oldUserData.Point[Convert.ToInt16(current_level) - 1][Convert.ToInt16(current_sublevel) - 1];

            DBHelper.UserSateModel newState = new DBHelper.UserSateModel();
            DBHelper.LevelInfo levelInfo = new DBHelper.LevelInfo();
            levelInfo.Level = 1;
            levelInfo.SubLevel = 1;
            levelInfo.Line = 1;
            newState.LevelInfo = levelInfo;

            String[] numChar = { "1️⃣", "2️⃣", "3️⃣", "4️⃣" };
            String retValue = "🔸 Level: " + current_level + "_" + current_sublevel + "," + " Quize: " + Quize[0] + "\n";
            retValue += "💠 " + Quize[1] + "\n";
            int trueAnswer = 0;
            for (int i = 2; i < Quize.Count; i++)
            {
                if (Quize[i].Equals(Quize[6]))
                {
                    trueAnswer = i - 2;
                    break;
                }
            }
            if(Quize[6].Equals(Quize[Answer + 2]))
            {
                for (int i = 0; i < 4; i++)
                {
                    if (Answer == i)
                    {
                        retValue += "\n" + "✅" + " " + Quize[i + 2];
                        newState.Point[Convert.ToInt16(current_level) - 1][Convert.ToInt16(current_sublevel) - 1] = last_point + TRUE_ANSWER_POINT;

                    }
                    else
                    {
                        retValue += "\n" + numChar[i] + " " + Quize[i + 2];
                    }
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    if(Answer == i)
                    {
                        retValue += "\n" + "❌" + " " + Quize[i + 2];
                    }
                    else if(trueAnswer == i)
                    {
                        retValue += "\n" + "✅" + " " + Quize[i + 2];
                        newState.Point[Convert.ToInt16(current_level) - 1][Convert.ToInt16(current_sublevel) - 1] = last_point + TRUE_ANSWER_POINT;
                    }
                    else
                    {
                        retValue += "\n" + numChar[i] + " " + Quize[i + 2];
                    }
                }
            }

            DBHelper.setUserState(chat_id, newState);

            return retValue;
        }

        private void ParseQuize(String text, out String current_level, out String current_sublevel, out String current_quize, out String total_quize)
        {
            String[] splitedQuize = text.Split('\n');

            String[] Level = splitedQuize[0].Replace(" Quize: ", "").Replace("🔸 Level: ", "").Split('_');
            String[] Sublevel = Level[1].Split(',');
            String[] Quize = Sublevel[1].Split('/');

            current_level = Level[0];
            current_sublevel = Sublevel[0];
            current_quize = Quize[0];
            total_quize = Quize[1];
        }

        private void ParseRepeat(String text, out String current_level, out String current_sublevel, out String current_repeate, out String total_repeat)
        {
            String[] splitedRepeat = text.Split('\n');

            String[] Level = splitedRepeat[0].Replace("🔸 Level: ", "").Split('_');
            String[] Sublevel = Level[1].Split(',');
            String[] Repeat = splitedRepeat[1].Replace("🎤 Repeat A: ", "").Replace("🎤 Repeat B: ", "").Replace("🎤 Repeat C: ", "").Split('/');

            current_level = Level[0];
            current_sublevel = Sublevel[0];
            current_repeate = Repeat[1];
            total_repeat = Repeat[0];
        }

        /// <summary>
        /// Create level by image url
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="level"></param>
        /// <param name="sublevel"></param>
        /// <returns></returns>

        private String CreateLevelImageUrl(long userid, int level, int sublevel, int points)
        {
            return "<a href=\"" + "http://bots.narcissoft.com/magicspeak/bot/LevelImage?" + "level=" + level.ToString() + "&subLevel=" + sublevel.ToString() + "&userid=" + userid.ToString() + "&points=" + points.ToString() + "\">🔸</a> Level: " + level.ToString() + "_" + sublevel.ToString();
        }
        
        /// <summary>
        /// Create single voice level url
        /// </summary>
        /// <param name="level"></param>
        /// <param name="sublevel"></param>
        /// <param name="totalCount"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        private String CreateVoiceUrl(String level, String sublevel, String totalCount, String file)
        {
            return "<a href=\"" + "http://bots.narcissoft.com/" + level.ToString() + sublevel.ToString() + file + ".ogg" + "\">🔸</a> Level: " + level + "_" + sublevel + "\n🎵 Voice: " + totalCount + '/' + file;
        }

        /// <summary>
        /// Read first line of received Inline Keyboard Markup text and parse it for retrieved current level and sublevel.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="current_level"></param>
        /// <param name="current_sublevel"></param>
        private void ParseImageLableLevel(String text, out String str_current_level, out String str_current_sublevel)
        {
            String[] current_level_info = text.Replace("🔸 Level: ", "").Split('_');
            str_current_level = current_level_info[0];
            str_current_sublevel = current_level_info[1];
        }

        /// <summary>
        /// Parse level when text has tag.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="str_current_level"></param>
        /// <param name="str_current_sublevel"></param>
        private void ParseHashTagedLevel(String text, out String str_current_level, out String str_current_sublevel)
        {
            String[] splited_text = text.Split('\n');
            String[] splited_level = splited_text[0].Split(' ');
            int iLevel = Convert.ToInt16(splited_level[1].Replace("#Level_", ""));
            int iSublevel = Convert.ToInt16(splited_level[2].Replace("#Sublevel_", ""));
            str_current_level = IntToString(iLevel);
            str_current_sublevel = IntToString(iSublevel);
        }

        /// <summary>
        /// Parse text of single voice
        /// </summary>
        /// <param name="text"></param>
        /// <param name="str_current_level"></param>
        /// <param name="str_current_sublevel"></param>
        /// <param name="str_total_count"></param>
        /// <param name="str_current_line"></param>
        private void ParseVideoCallback(String text, out String str_current_level, out String str_current_sublevel)
        {
            String tmp = "#Video #Level_1 #Sublevel_2";
            String[] splited_text = text.Split(' ');
            str_current_level = splited_text[1].Replace("#Level_", "");
            str_current_sublevel = splited_text[2].Replace("#Sublevel_", "");
        }

        private void ParseRepeatCallback(String text, out String str_current_level, out String str_current_sublevel, out String str_current_part)
        {
            String[] splited_text = text.Split('\n');
            String[] current_level_info = splited_text[0].Replace("🔸 Level: ", "").Split('_');
            str_current_level = current_level_info[0];
            str_current_sublevel = current_level_info[1];
            str_current_part = splited_text[1].Replace("🎤 Repeat ", "");
        }

        private void HandleLevelMenuByPoint(TelegramTypes.X_Update xCallback, int current_level, int current_sublevel, int points)
        {
            TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();
            long chat_id = xCallback.callback_query.from.id;
            long message_id = xCallback.callback_query.message.message_id;
            if (points == -1)
            {
                inlineKeyboardMarkup = CreateLevelMenu(false);
                _telegram_interface.editMessageText(chat_id, message_id, CreateLevelImageUrl(chat_id, current_level, current_sublevel, points), inlineKeyboardMarkup, "HTML");
            }
            else
            {
                inlineKeyboardMarkup = CreateLevelMenu(true);
                _telegram_interface.editMessageText(chat_id, message_id, CreateLevelImageUrl(chat_id, current_level, current_sublevel, points), inlineKeyboardMarkup, "HTML");
            }
        }

        private String getMaxConfidenceSentence(GoogleTypes.X_Json_Google_Speech_Result google_result)
        {
            double maxConfidence = 0;
            int iMaxConfidence = 0;
            for (int i = 0; i < google_result.alternatives.Count; i++)
            {
                if (maxConfidence < google_result.alternatives[i].confidence)
                {
                    maxConfidence = google_result.alternatives[i].confidence;
                    iMaxConfidence = i;
                }
            }
            return google_result.alternatives[iMaxConfidence].transcript;
        }

        private int CalcLevenshteinDistance(string str_a, string str_b)
        {
            if (String.IsNullOrEmpty(str_a) || String.IsNullOrEmpty(str_b)) return 0;

            int lengthA = str_a.Length;
            int lengthB = str_b.Length;
            var distances = new int[lengthA + 1, lengthB + 1];
            for (int i = 0; i <= lengthA; distances[i, 0] = i++) ;
            for (int j = 0; j <= lengthB; distances[0, j] = j++) ;

            for (int i = 1; i <= lengthA; i++)
                for (int j = 1; j <= lengthB; j++)
                {
                    int cost = str_b[j - 1] == str_a[i - 1] ? 0 : 1;
                    distances[i, j] = Math.Min
                        (
                        Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
                        distances[i - 1, j - 1] + cost
                        );
                }
            return distances[lengthA, lengthB];
        }

        private void getVoiceOfDialogWithForceReplay(long chat_id, String current_level, String current_sublevel, String current_part, String total_repeat, String current_repeat, TelegramTypes.X_InlineKeyboardMarkup replymarkup = null)
        {
            int iTotal_repeat = Convert.ToInt16(total_repeat);
            int iCurrent_repeat = Convert.ToInt16(current_repeat);
            if(iCurrent_repeat <= iTotal_repeat)
            {
                TelegramTypes.Action_ForceReply force_replay_msg = new TelegramTypes.Action_ForceReply
                {
                    force_reply = true
                };
                switch(current_part)
                {
                    case "A":
                        _telegram_interface.SendMessage(chat_id,
                            "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + current_part + ": " + total_repeat + '/' + current_repeat
                            + "\n\n"
                            + "متن زیر را تکرار کنید" + "\n" + DBHelper.getConversionLine(Convert.ToInt16(current_level), Convert.ToInt16(current_sublevel), Convert.ToInt16(current_repeat))
                            , force_replay_msg, false, false, 0, "HTML");
                        break;

                    case "B":
                        _telegram_interface.SendMessage(chat_id,
                                "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + current_part + ": " + total_repeat + '/' + current_repeat
                                + "\n\n"
                                + "به متن زیر پاسخ مناسب دهید." + "\n" + DBHelper.getConversionLine(Convert.ToInt16(current_level), Convert.ToInt16(current_sublevel), Convert.ToInt16(current_repeat))
                                , force_replay_msg, false, false, 0, "HTML");
                        
                        break;

                    case "C":
                        if (iCurrent_repeat == iTotal_repeat)
                        {
                            force_replay_msg = new TelegramTypes.Action_ForceReply
                            {
                                force_reply = false
                            };
                            _telegram_interface.SendMessage(chat_id,
                                "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + current_part + ": " + total_repeat + '/' + current_repeat
                                + "\n\n"
                                + DBHelper.getConversionLine(Convert.ToInt16(current_level), Convert.ToInt16(current_sublevel), Convert.ToInt16(current_repeat))
                                , force_replay_msg, false, false, 0, "HTML");
                        }
                        else
                        {
                            _telegram_interface.SendMessage(chat_id,
                                "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + current_part + ": " + total_repeat + '/' + current_repeat
                                + "\n\n"
                                + "به متن زیر پاسخ مناسب دهید." + "\n" + DBHelper.getConversionLine(Convert.ToInt16(current_level), Convert.ToInt16(current_sublevel), Convert.ToInt16(current_repeat))
                                , force_replay_msg, false, false, 0, "HTML");
                        }
                        break;
                }
                
            }
            else
            {
                if(replymarkup != null)
                {
                    switch (current_part)
                    {
                        case "A":
                            _telegram_interface.SendMessage(chat_id,
                        "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + current_part
                        + "\n\n"
                        + "مرحله اول به پایان رسید در مرحله بعدی من جای نفر اول صحبت می کنم و شما جملات نفر دوم رو از حفظ تکرار کن.", replymarkup);
                            break;

                        case "B":
                            _telegram_interface.SendMessage(chat_id,
                        "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + current_part
                        + "\n\n"
                        + "مرحله دوم به پایان رسید در مرحله بعدی من جای نفر دوم صحبت می کنم و شما جملات نفر اول رو از حفظ تکرار کن.", replymarkup);
                            break;

                        case "C":
                            _telegram_interface.SendMessage(chat_id,
                        "🔸 Level: " + current_level + "_" + current_sublevel + "\n🎤 Repeat " + current_part
                        + "\n\n"
                        + "مرحله اول به پایان رسید در مرحله بعدی من جای نفر اول صحبت می کنم و شما جملات نفر دوم رو از حفظ تکرار کن.", replymarkup);
                            break;
                    }
                    
                }
                else
                {
                    _telegram_interface.SendMessage(chat_id, "NotImplementedException: NI003");
                }
            }
            
        }

        /// <summary>
        /// Convert integer value to string by adding 0 for number is less than 9.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private String IntToString(int number)
        {
            String retValue = "";
            if (number <= 9)
            {
                retValue = ("0" + number.ToString());
            }
            else
            {
                retValue = number.ToString();
            }
            return retValue;
        }

        
    }
}