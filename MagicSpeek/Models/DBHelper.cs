﻿using MagicSpeek.TelegramHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace MagicSpeek.Models
{
    public class DBHelper
    {
        public static List<string> getConversation(int current_level, int current_sublevel)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("MagicSpeak");
            // set the stored procedure name
            comm.CommandText = "GetConversation";

            // create the LEVELNO parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@LevelNo";
            param.Value = current_level;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = current_sublevel;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            DataTable dtConversation = GenericDataAccess.ExecuteSelectCommand(comm);

            List<string> conversion = new List<string>();
            for (int i = 0; i < dtConversation.Rows.Count; i++)
            {
                conversion.Add(dtConversation.Rows[i]["Context"].ToString());
            }
            //conversion.Add("Jessica: Did you hear about Molly and Dan?");
            //conversion.Add("Diane: No, what's going on?");
            //conversion.Add("Jessica: They're getting a divorce.");
            //conversion.Add("Diane: Really? They've been married for 30 years.");
            //conversion.Add("Jessica: Dan cheated on Molly.");
            //conversion.Add("Diane: That is awful!");

            return conversion;
        }

        public static string getConversionLine(int current_level, int current_sublevel, int LineNo)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("MagicSpeak");
            // set the stored procedure name
            comm.CommandText = "GetConversationLine";

            // create the LEVELNO parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@LevelNo";
            param.Value = current_level;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = current_sublevel;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the  LINE NO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LineNo";
            param.Value = LineNo;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            return GenericDataAccess.ExecuteScalar(comm);

            //List<string> conversion = new List<string>();
            //conversion.Add("Jessica: Did you hear about Molly and Dan?");
            //conversion.Add("Diane: No, what's going on?");
            //conversion.Add("Jessica: They're getting a divorce.");
            //conversion.Add("Diane: Really? They've been married for 30 years.");
            //conversion.Add("Jessica: Dan cheated on Molly.");
            //conversion.Add("Diane: That is awful!");
            //return conversion[LineNo - 1];
        }

        public static string getConversionVoice(long chat_id, int current_level, int current_sublevel)
        {
            String voice_link = "http://bots.narcissoft.com/311.ogg";
            return voice_link;
        }

        public static List<string> GetQuizeInfo(int level, int sublevel, int quizeNo)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("MagicSpeak");
            // set the stored procedure name
            comm.CommandText = "GetQuizeInfo";

            // create the QuizeNo parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@QuizeNo";
            param.Value = quizeNo;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the LEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LevelNO";
            param.Value = level;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // create the SUBLEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@SublevelNo";
            param.Value = sublevel;
            param.DbType = DbType.Int16;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            DataTable dtUserData = GenericDataAccess.ExecuteSelectCommand(comm);

            List<string> QuizeInfo = new List<string>();

            if (dtUserData.Rows.Count > 0)
            {
                QuizeInfo.Add(dtUserData.Rows[0]["QuizeNo"].ToString());
                QuizeInfo.Add(dtUserData.Rows[0]["Question"].ToString());

                string[] Answers = Regex.Split(dtUserData.Rows[0]["Answers"].ToString(), "\r\n");
                foreach (string answer in Answers)
                {
                    QuizeInfo.Add(answer);
                }
                QuizeInfo.Add(dtUserData.Rows[0]["Answer"].ToString());
            }
            else
            {
            }

            return QuizeInfo;
        }


        //public static UserSateModel getUserState(long chat_id)
        //{
        //    //At first retrieve json string from DB and serialize to UserSateModel
        //    UserSateModel userState = new UserSateModel();
        //    LevelInfo levelInfo = new LevelInfo()
        //    {
        //        LevelName = LevelName.GOOGLE_REPEAT_A,
        //        Level = 2,
        //        SubLevel = 2,
        //        Line = 5
        //    };
        //    userState.LevelInfo = levelInfo;
        //    userState.VideoSpeed = "NORMAL";
        //    userState.Point = 50;

        //    return userState;
        //}

        //public static void setUserState(long chat_id, UserSateModel userState)
        //{
        //    string json = JsonConvert.SerializeObject(userState, Formatting.None,
        //                new JsonSerializerSettings
        //                {
        //                    NullValueHandling = NullValueHandling.Ignore
        //                });
        //    //At the end save json in DB.
        //}

        public static UserSateModel getUserState(long user_id, long nInvitedBy, string firstname, string lastname, string username, out bool isNewUser)
        {
            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("NarBots");
            // set the stored procedure name
            comm.CommandText = "MagicSpeakGetUserState";

            // create the USERID parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@UserID";
            param.Value = user_id;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the output ISNEWUSER parameter
            param = comm.CreateParameter();
            param.ParameterName = "@IsNewUser";
            param.Direction = ParameterDirection.Output;
            param.DbType = DbType.Int32;
            comm.Parameters.Add(param);

            // create the INVEITEBY parameter
            param = comm.CreateParameter();
            param.ParameterName = "@InvitedByID";
            param.Value = nInvitedBy;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the FIRSTNAME parameter
            param = comm.CreateParameter();
            param.ParameterName = "@FisrtName";
            param.Value = firstname;
            param.DbType = DbType.String;
            comm.Parameters.Add(param);

            // create the LASTNAME parameter
            param = comm.CreateParameter();
            param.ParameterName = "@LastName";
            param.Value = lastname;
            param.DbType = DbType.String;
            comm.Parameters.Add(param);

            // create the USERNAME parameter
            param = comm.CreateParameter();
            param.ParameterName = "@UserName";
            param.Value = username;
            param.DbType = DbType.String;
            comm.Parameters.Add(param);

            // execute the stored procedure and return the results
            string result = GenericDataAccess.ExecuteScalar(comm);
            UserSateModel userState = null;

            if (result == "NaN")
            {
                isNewUser = true;
            }
            else
            {
                isNewUser = false;
                userState = JsonConvert.DeserializeObject<UserSateModel>(result);
            }

            /**********************************************************************/
            userState = new UserSateModel();
            List<List<int>> newPoints = new List<List<int>>();
            List<int> SubLevelPoints = new List<int>();
            for (int i = 0; i < 20; i++)
            {
                SubLevelPoints.Add(-1);
            }
            for (int i = 0; i < 10; i++)
            {
                newPoints.Add(SubLevelPoints);
            }
            SubLevelPoints[0] = 0;
            SubLevelPoints[1] = 0;
            newPoints[0] = SubLevelPoints;
            DBHelper.LevelInfo levelInfo = new DBHelper.LevelInfo();
            levelInfo.Level = 1;
            levelInfo.SubLevel = 1;
            levelInfo.Line = 1;
            userState.LevelInfo = levelInfo;
            userState.Point = newPoints;
            userState.LevelName = "NaN";
            /**********************************************************************/

            return userState;
        }

        public static int setUserState(long user_id, UserSateModel userState)
        {
            string json = JsonConvert.SerializeObject(userState, Formatting.None,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

            // now the end save json in DB.

            // get a configured DbCommand object
            SqlCommand comm = GenericDataAccess.CreateCommand("NarBots");
            // set the stored procedure name
            comm.CommandText = "MagicSpeakSetUserState";

            // create the USERID parameter
            SqlParameter param = comm.CreateParameter();
            param.ParameterName = "@UserID";
            param.Value = user_id;
            param.DbType = DbType.Int64;
            comm.Parameters.Add(param);

            // create the LEVELNO parameter
            param = comm.CreateParameter();
            param.ParameterName = "@UserState";
            param.Value = json;
            param.DbType = DbType.String;
            comm.Parameters.Add(param);

            return GenericDataAccess.ExecuteNonQuery(comm);
        }


        public static class LevelName
        {
            public static string CHOOSE_LEVEL = "CHOOSE_LEVEL";
            public static string GOOGLE_REPEAT_A = "GOOGLE_REPEAT_A";
            public static string GOOGLE_REPEAT_B = "GOOGLE_REPEAT_B";
            public static string GOOGLE_REPEAT_C = "GOOGLE_REPEAT_C";
            public static string VOICE = "VOICE";
        }

        public class LevelInfo
        {
            public int Level { get; set; }
            public int SubLevel { get; set; }
            public int Line { get; set; }
        }

        public class UserSateModel
        {
            public LevelInfo LevelInfo { get; set; }
            public string VideoSpeed { get; set; }
            public List<List<int>> Point { get; set; }
            public string LevelName { get; set; }
        }

        internal static string getConversionVideo(long chat_id, String current_level, String current_sublevel)
        {
            return "MS.narcissoft.com/media/videos/" + current_level + current_sublevel + ".mp4";
        }

        public static void SaveLevelInfo(long chat_id, TelegramTypes.X_Update xCallback, String new_str_current_level, String new_str_current_sublevel, int newLine = 1, string newLevelName = "NaN")
        {
            bool f_isNewUser = false;
            UserSateModel newUserState = getUserState(chat_id, 0, xCallback.callback_query.from.first_name, xCallback.callback_query.from.last_name, xCallback.callback_query.from.username, out f_isNewUser);
            DBHelper.LevelInfo levelInfo = new DBHelper.LevelInfo();
            levelInfo.Level = Convert.ToInt16(new_str_current_level);
            levelInfo.SubLevel = Convert.ToInt16(new_str_current_sublevel);
            levelInfo.Line = newLine;
            newUserState.LevelInfo = levelInfo;
            newUserState.Point = newUserState.Point;
            newUserState.LevelName = newLevelName;
            DBHelper.setUserState(chat_id, newUserState);
        }

    }
}