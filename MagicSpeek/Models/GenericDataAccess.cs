﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace MagicSpeek.Models
{
    public class GenericDataAccess
    {
        // executes a command and returns the results as a DataTable object
        public static DataTable ExecuteSelectCommand(SqlCommand command)
        {
            // The DataTable to be returned
            DataTable table;
            // Execute the command, making sure the connection gets closed in the
            // end
            try
            {
                // Open the data connection
                command.Connection.Open();
                // Execute the command and save the results in a DataTable
                SqlDataReader reader = command.ExecuteReader();
                table = new DataTable();
                table.Load(reader);
                // Close the reader
                reader.Close();
            }
            catch (Exception ex)
            {
                // Log the error here EX: Utilities.LogError(ex);
                throw;
            }
            finally
            {
                // Close the connection
                command.Connection.Close();
            }

            return table;
        }

        // creates and prepares a new DbCommand object on a new connection
        public static SqlCommand CreateCommand(string Name)
        {
            // Create the connection object
            SqlConnection conn = new SqlConnection
            {
                ConnectionString = WebConfigurationManager.ConnectionStrings[Name].ConnectionString
            };

            // Set the connection string

            // Create a command object
            SqlCommand comm = conn.CreateCommand();

            // Set the command type to stored procedure
            comm.CommandType = CommandType.StoredProcedure;

            // Return the initialized command object
            return comm;
        }

        public static int ExecuteNonQuery(SqlCommand command)
        {
            // The number of affected rows
            int affectedRows = -1;
            // Execute the command making sure the connection gets closed in the end
            try
            {
                // Open the data connection
                command.Connection.Open();
                // Execute the command and get the number of affected rows
                affectedRows = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the error here EX: Utilities.LogError(ex);
                throw;
            }
            finally
            {
                // Close the connection
                command.Connection.Close();
            }

            return affectedRows;
        }

        public static string ExecuteScalar(DbCommand command)
        {
            // The value to be returned
            string value = "";
            // Execute the command making sure the connection gets closed in the end
            try
            {
                // Open the connection of the command
                command.Connection.Open();
                // Execute the command and get the number of affected rows
                var objResult = command.ExecuteScalar();
                if (objResult != null)
                    value = objResult.ToString();
            }
            catch (Exception ex)
            {
                // Log eventual errors and rethrow them
                // Utilities.LogError(ex);
                throw;
            }
            finally
            {
                // Close the connection
                command.Connection.Close();
            }
            // return the result
            return value;
        }
    }
}